%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\subsection {Versions Kontrollsysteme}
\label{sec:vcs}

Nachdem in den vorhergehenden Kapiteln ausführlich auf die verwendete Hardware
eingegangen wurde, sollen sich die nun folgenden Abschnitte mit den Software
Komponenten beschäftigen, die nötig sind, um eine Entwicklungsplattform für
Embedded Linux aufzubauen. Neben den tatsächlich später verwendeten
Komponenten, wie dem Linux Kernel oder den Bootloadern, zählen dazu auch einige
wichtige Software Entwicklerwerkzeuge wie die sogenannten \acf{VCS}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{git-vcs}
  \caption{Klassische \ac{VCS}}
  \label{fig:git-vcs}
\end{figure}

\ac{VCS} werden in der Softwareentwicklung schon seit Jahrzehnten eingesetzt.
Sie übernehmen klassischerweise die Aufgabe, den Stand einer Software, die sich
über die Zeit hinweg stetig weiterentwickelt, reproduzierbar festzuhalten.
Dadurch ergibt sich die Möglichkeit, unterschiedliche Revisionen einer Software
auf Ebene des Quelltextes miteinander zu vergleichen (\autoref{fig:git-vcs}).
Prominente Vertreter dieser klassischen \ac{VCS} sind zum Beispiel RCS, CVS
oder das modernere Subversion \cite{wiki:vcs}.

Neben der reinen Revisionsverwaltung bieten die meisten klassischen \ac{VCS}
auch die Möglichkeit mittels einer Client-Server Architektur, den Zugriff
mehrerer Entwickler auf ein zentrales Software Repository zu koordinieren und
unterstützen damit das gemeinsame Arbeiten an einer Software
(\autoref{fig:git-vcs-central}). Während diese Funktionalität bei den älteren
\ac{VCS} wie RCS und CVS nachträglich hinzugefügt wurde, ist sie bei modernen
Systemen wie Subversion direkt integriert.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{git-vcs-central}
  \caption{Zentralisierte Architektur klassischer \ac{VCS}}
  \label{fig:git-vcs-central}
\end{figure}

Gerade für große Software Projekte, wie dem Linux Kernel, an dem hunderte oder
tausende von Entwicklern gleichzeitig arbeiten, haben sich die koordinativen
Möglichkeiten eines \ac{VCS} im Laufe der Zeit als wichtigstes Kriterium dieser
Systeme herauskristallisiert. Die zentralistische Architektur traditioneller
\ac{VCS} stellt dabei ein konzeptionelles Hindernis dar \cite{chacon2009pro}.

Aus dieser Motivation heraus entwickelte sich in den letzten fünf Jahren eine
neue Generation von \ac{VCS} die sogenannten \ac{DVCS}. Inzwischen spielen
\ac{DVCS} bei den meisten großen OpenSource Projekten eine zentrale Rolle in
der Organisation und Koordination der Software Entwicklung. Deswegen ist es
auch für Unternehmen, welche diese Software für eigene Produkte nutzen möchten,
eine wichtige Kernkompetenz geworden, diese Werkzeuge zu verstehen und nutzen
zu können.

\subsection {\texttt{git}, ein modernes Verteiltes Versions Kontrollsystem}
\label {sec:git}

Ein wichtiger Vertreter der in \autoref{sec:vcs} vorgestellten \ac{DVCS} ist
\texttt{git}. Obwohl es inzwischen eine große Auswahl unterschiedlicher freier
und auch kommerzieller \ac{DVCS} Systeme gibt, ist \texttt{git} sicher das am
weitesten verbreitete System. Da \texttt{git} im weiteren Verlauf dieser Arbeit
eine wichtige Rolle spielen wird, soll in diesem Abschnitt ein grundlegendes
Verständnis für die Konzepte dieser Software geschaffen werden.

\begin{figure}[ht]
  \begin{minipage}[b]{0.45\linewidth}
  \centering
  \includegraphics[width=\textwidth]{git-vcs-store}
  \caption*{Traditionelle \ac{VCS}}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[b]{0.45\linewidth}
  \centering
  \includegraphics[width=\textwidth]{git-dvcs-store}
  \caption*{\texttt{git}}
  \end{minipage}
  \caption{Speicherung einzelner Revisionen im Vergleich}
  \label{fig:vcs-store}
\end{figure}

Alle traditionellen \ac{VCS} speichern als Revisionen einer Software den
Unterschied einzelner Dateien, \texttt{git} dagegen betrachtet ein Software
Repository wie ein Dateisystem und speichert für jede Revision einen kompletten
Snapshot dieses gesamten Dateisystems (Siehe \autoref{fig:vcs-store}). Dieser
Art der Speicherung unterscheidet \texttt{git} grundsätzlich von fast allen
anderen \ac{VCS} und ermöglicht erst einige der mächtigen Features, die es so
populär werden ließen.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{git-local}
  \caption{Verteilte \texttt{git} Repositories}
  \label{fig:git-local}
\end{figure}

Das \texttt{git} als \ac{DVCS} bezeichnet wird, rührt von der Tatsache, dass
es, im technischen Sinne, kein zentrales Repository gibt. Jede Kopie eines
\texttt{git}-Repositories beinhaltet die gesamte, zum Zeitpunkt der Erstellung
verfügbare Historie. Aus diesem Grund können alle Repositories untereinander
Revisionen austauschen (\autoref{fig:git-local}). Das Anlegen einer Kopie wird
im \texttt{git}-Terminus folglich auch als \enquote{clonen} bezeichnet.

Diese verteilte Struktur ist auch der Grund warum nahezu alle \ac{VCS} üblichen
Kommandos unter \texttt{git} rein lokal ablaufen und für das Arbeiten in einem
Repository keine Netzwerkverbindung notwendig ist. Ein willkommener Nebeneffekt
dieser Tatsache ist, das Vermeiden von Netzwerklatenzen und somit eine sehr
hohe Geschwindigkeit bei den meisten Aktionen.

Die in einem \texttt{git}-Repository gespeicherten Revisionen beinhalten immer
einen Pointer auf die vorhergehende Revision (bei mehreren Revisionen auch
mehrere Pointer); so ergibt sich die Historie eines Repositories als
sogenannter \ac{DAG}\cite{wiki:dag}. Zu Beginn eines neuen Repositories wird
ein spezieller Pointer angelegt, der immer auf die aktuelle Revision zeigt, der
\texttt{master} Branch (\autoref{fig:git-snapshots}).

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{git-snapshots}
  \caption{\texttt{git}-Graph einzelner Revisionen mit \texttt{master}-Pointer}
  \label{fig:git-snapshots}
\end{figure}

Eine solche Branch steht also immer für einen gewählten Endpunkt innerhalb
eines \texttt{git}-\ac{DAG} und repräsentiert damit die komplette Historie
dieses Branch. Durch dieses einfache Verfahren ist das Erzeugen einer Branch
unter \texttt{git} extrem einfach und schnell. Aus diesem Grund basieren fast
alle Entwicklungsmodelle für die \texttt{git} genutzt wird auf exzessivem
\enquote{branching}. \autoref{fig:git-branch} zeigt einen einfachen
\texttt{git}-\ac{DAG} mit zwei Branches. Der dritte \texttt{HEAD}-Branch ist
ein zusätzlicher Pointer und markiert den aktuell genutzten Branch.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{git-branch}
  \caption{\texttt{git}-Graph mit zwei Branches und dem \texttt{HEAD}-Pointer}
  \label{fig:git-branch}
\end{figure}

\subsection{\texttt{git-flow}, ein git Entwicklungsmodell}
\label{sec:git-flow}

Um zu demonstrieren, wie mittels der in \autoref{sec:git} vorgestellten Technik
des \enquote{branching} eine verteilte Software Entwicklung organisiert werden
kann, soll hier das am weitesten verbreitete \texttt{git} branching Model
\texttt{git-flow} vorgestellt werden. git-flow wurde Anfang 2010 von Vincent
Driessen, einem Software Entwickler aus den Niederlanden, erdacht und ist nicht
nur deshalb so weit verbreitet, weil es sehr gut strukturiert ist, sondern
auch, weil Driessen es in Form einer \texttt{git} Erweiterung in Software
gegossen hat\footnote{Siehe: \url{https://github.com/nvie/gitflow}}
\cite{driessen2010}.

Ein Branching-Modell legt standardmäßig fest, wie Branches benannt werden
sollen und wohin sie gemerged werden. Sie spezifizieren was eine stabile
Version ist und definieren die generelle Struktur der Zusammenarbeit unter den
Softwareentwicklern.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{git-flow}
  \caption{Das Software Entwicklungsmodelle \texttt{git-flow}}
  \label{fig:git-flow}
\end{figure}

\texttt{git-flow} spezifiziert zwei Branches, die immer Bestand haben,
\texttt{master} und \texttt{develop}. Jede Revision in Master ist, per
Definition, eine stabile Version der Software. In der \texttt{develop} Branch
werden die neuen Releases vorbereitet und nach \texttt{master} gemerged.
Außerdem gibt es noch \texttt{hotfix}, \texttt{feature} und \texttt{release}
Branches. \texttt{hotfix} Branches basieren auf dem \texttt{master} Branch und
werden nach \texttt{master} und \texttt{develop} gemerged. \texttt{feature}
Branches basieren auf dem \texttt{develop} Branch und werden wieder nach
\texttt{develop} gemerged.  Und die \texttt{release} Branches letztendlich
basieren auf dem \texttt{develop} Branch und werden nach \texttt{master} und
\texttt{develop} gemerged. Alle diese Branches haben, außer der Konvention, die
mit ihnen verknüpft ist, keine Besonderheit im technischen Sinne.
\autoref{fig:git-flow} veranschaulicht den \enquote{Workflow} von
\texttt{git-flow}.

\footnotetext{Bilderquelle: \url{http://git-scm.com}}

% vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab:
