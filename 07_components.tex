%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\subsection {Komponenten eines Embedded Linux Systems}
\label{sec:comp}

Im \autoref{sec:buildsys} wurden die Kernkomponenten eines Embedded Linux
Systems schon kurz angerissen. Dieser Abschnitt soll die einzelnen Komponenten
eines solchen Systems detailliert erläutern. Um zu verstehen, was nötig ist, um
ein Embedded Linux System zu starten, muss zuerst der Bootprozess besprochen
werden.  Prinzipiell sollte das hier besprochene Bootschema auch auf andere
CPU-Boards anwendbar sein. Die nachfolgende Liste zeigt die einzelnen
Bootschritte auf \cite{atmel:sam9g20}:

\begin{enumerate}[label=\arabic*.,itemsep=0.5ex]
  \item Stomversorgung wird eingeschaltet.
  \item Debug Unit serial port (DBGU) und der USB High Speed Device Port
    werden initialisiert.
  \item Das Atmel Bootprogramm wird gestartet und sucht nach externen
    Speichermedien. Im Falle des Stamp9G20 ist nur ein NAND Flash Speicher
    angeschlossen.
  \item Es werden die ersten 28 Bytes des NAND Flash Speichers eingelesen (Ab
    Addresse 0x40000000). Diese müssen einen gültigen Vector mit 7 ARM LDR oder
    B-Opcode Instruktionen enthalten.
  \item Der 6. Wert des Vectors muss die Länge des \ac{SPL} Programms
    enthalten.
  \item Das Atmel NAND Boot Programm liest das \ac{SPL} aus dem NAND Flash
    Speicher in den SRAM der CPU und startet es. Da der SRAM nur 16Kb groß
    ist, darf das \ac{SPL} ebenfalls maximal 16Kb groß sein (abzüglich 28 Byte
    ARM Obcode).
  \item Das \ac{SPL} initialisiert den SDRAM, lädt den Bootloader aus dem NAND
    Flash in den SDRAM und startet diesen.
  \item Der Bootloader lädt einige Funktionsbibliotheken, relokalisiert sich
    selbst ans obere Ende des RAM-Speichers und initialisiert zusätzliche
    Hardware Komponenten. Danach lädt es den Linux Kernel, schreibt ihn ans
    untere Ende des RAM-Speichers und startet ihn.
  \item Der Linux Kernel bootet, initialisiert den Rest der Hardware, mounted
    das \ac{RootFS} und startet den init-Prozess.
  \item Der init-Prozess startet verschiedene System Applikationen und beendet
    damit den Bootprozess. Das Embedded Linux System ist jetzt vollständig
    geladen.
\end{enumerate}

Ein wichtiger Teil dieser Arbeit war es, diesen Bootprozess zu verstehen und
auch Details, wie die Lokalisation der einzelnen Komponenten, in den
unterschiedlichen Speichermedien des Stamp9M20 Boards so zu planen, dass sich
die einzelnen Komponenten nicht gegenseitig stören.

\subsection {Das \ac{SPL} AT91Bootstrap}
\label{sec:bootstrap}

Die erste Komponente dieses Bootprozesses, die hier besprochen werden soll, ist
das \ac{SPL} AT91Bootstrap von Atmel. Diese Software wird von Atmel frei und im
Quellcode bereitgestellt und dient als Referenzimplementierung für ein
\ac{SPL}. Grundsätzlich stellt diese Software alle nötigen Funktionen eines
Boodloader bereit:

\begin{itemize}
  \item Initialisierung verschiedener Hardware Komponenten wie Clocks, SDRAM
    Controller und Flash-Speicher.
  \item \acs{PIO} und \acs{PCM} Treiber.
  \item Zugriff auf Speichermedien wie NAND und Data Flash.
  \item Unterstützung verschiedener Dateisysteme wie \acs{JFFS2} und FAT.
  \item Das Starten von beliebigen \acs{ELF} Applikationen und des Linux
    Kernels.
\end{itemize}

Dass dieses Programm, trotz dieser eigentlich beeindruckenden Liste an
Funktionalitäten, meist nur als \ac{SPL} für einen Second Stage Bootloader wie
U-Boot verwendet wird, liegt vor allem daran, dass Atmel diese Software leider
nicht zentral weiterentwickelt. Das hat zur Folge, dass wichtige
Funktionalitäten, wie z.B. das Booten aus dem Netzwerk fehlen und Hardware
Komponenten, wie neuere NAND-Chips, nicht mehr unterstützt werden.

Ein weiteres Problem dieser Software ist, dass im Internet zahlreiche Versionen
dieser Software kursieren, die zum Teil von Firmen oder OpenSource Projekten
weiterentwickelt wurden, aber zueinander inkompatibel sind. Da dieses Phänomen
nicht nur bei der Bootstrap Software von Atmel existent ist, entwickeln
Projekte wie \enquote{Das U-Boot} inzwischen eigene \ac{SPL} Software für die
unterstützten Boards. Dazu aber in \autoref{sec:uboot} mehr.

taskit liefert den PortuxG20 und das Stamp9G20 Modul mit einem vorinstallieren
AT91\-Boot\-strap aus, welcher auf der Version 3.0 von Atmel basiert, aber von
taskit angepasst wurde. Leider sind diese Anpassungen nicht dokumentiert und
deshalb schwer nachzuvollziehen. Ein weiteres Problem, welches das Einbinden
dieser angepassten Software in eine Build-Plattform erheblich erschwert, ist
die Tatsache, dass sich die Software nur mit einer sehr exotischen
Cross-Compilation Toolchain übersetzen lässt.

Um nicht auf die binäre Version von taskit angewiesen zu sein, war es
dementsprechend notwendig, eine Version der at91bootstrap Software zu finden,
die sich mit der Toolchain des Poky Buildsystems kompilieren lässt und
entsprechende Anpassungen vorzunehmen, damit diese auf dem Stamp9G20 Board
lauffähig ist.

\subsection {\enquote{Das U-Boot}, ein second stage Bootloader}
\label{sec:uboot}

\enquote{Das U-Boot} ist ein Embedded Device Bootloader für unterschiedliche
CPU-Plattformen. Es unterstützt einige hundert CPU-Boards und ist der wohl am
weitesten verbreitete Bootloader für Embedded Linux \cite{yaghmour2009}. Seine
Entwicklung wird seit 2002 von Wolfgang Denk koordiniert, die aktuelle Version
ist unter \url{http://git.denx.de} zu finden.

Die Entwicklung der U-Boot Software wird von einer gut strukturierten
OpenSource Entwicklergemeinde vorangetrieben. Das dabei eingesetzte Software
Development Modell ist an den bekannten \enquote{Dictator and Lieutenant}
Workflow angelehnt, der so ähnlich auch bei der Entwicklung des Linux Kernels
zur Anwendung kommt \cite{chacon2009pro}. Dazu gibt es zu jeder unterstützten
CPU-Board Familie einen Fork des U-Boot \texttt{git}-Repository, die
sogenannten \enquote{Custodian Repositories}. Für manche dieser Custodians
wiederum gibt es noch einmal spezialisierte Forks oder \enquote{Custodian
Repositories}. \autoref{fig:uboot-custodian} veranschaulicht die sich daraus
ergebende Baumstruktur.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.9\textwidth]{uboot-custodian}
  \caption{Baumstruktur der U-Boot custodian Repositories}
  \label{fig:uboot-custodian}
\end{figure}

Jede dieser Custodian Repositories wird von einem Lieutenant betreut. Die
Aufgabe dieses Lieutenants ist es, die Qualität der eingereichten Patches zu
prüfen und gelegentlich, sobald ein neuer stabiler Zustand des Repositories
erreicht ist, die Änderungen an das Mainline Repository weiterzuleiten.
Außerdem ist es seine Aufgabe, Updates, die das Mainline Repository von anderen
Custodian Repositories erreichen, per Pull oder Rebase zu übernehmen. Der
Maintainer des Mainline Repositories ist Wolfgang Denk, er fungiert als
Diktator und entscheidet, was in Mainline übernommen wird und somit auch alle
anderen Repositories erreicht.

Der grundsätzliche interne Aufbau des U-Boots orientiert sich stark am Beispiel
des Linux Kernels, mit Berücksichtigung der besonderen Einschränkungen eines
Bootloaders (beschränkter Speicher, uninitialisierte Hardware, etc.). Eine
Besonderheit ist die Fähigkeit, sich nach dem Initialisieren des RAM-Speichers
selbst an das obere Ende des Speichers zu relokalisieren. Dadurch können
nachfolgende Programme, wie z.B. der Linux Kernel, vom Anfang des RAM-Speichers
aus gestartet werden, was die Speicherverwaltung dieser Programme in der
Startphase sehr viel einfacher gestaltet.

\enquote{Das U-Boot} ist kein Programm, das einfach kompiliert und installiert
werden kann. Da ein Bootloader direkt auf der Hardware ausgeführt wird und
nicht auf standardisierte Betriebssystem Schnittstellen zurückgreifen kann, ist
jede kompilierte Version dieser Software speziell auf das entsprechende
CPU-Board angepasst. Dementsprechend existieren für jedes unterstützte Board
eine Reihe von Quellcode Dateien, die aus dem ganzen Arsenal an Funktionalität,
welche \enquote{Das U-Boot} bereitstellt die notwendigen Teile zusammenfassen
und sich zu einem startbaren ELF-Binary kompilieren lassen. Um wenigstens
einige Aspekte des U-Boots im Nachhinein konfigurierbar zu machen, gibt es die
Möglichkeit, einen Teil des Speichermediums zu nutzen um dort Variablen
abzulegen.

taskit liefert das Stamp9G20 CPU-Board und ebenso den PortuxG20 mit einer
vorinstallierten Version des U-Boots aus. Leider ist diese Version über zwei
Jahre alt und die Unterstützung der Stamp9G20 Plattform ist nicht Teil des
offiziellen U-Boot Repository Baums, sondern basiert auf einem Patch der Fa.
taskit. Leider lässt sich dieser Patch aufgrund der erheblichen internen
Umstrukturierungen der U-Boot Software in den letzten zwei Jahren nicht mehr
anwenden.

Eine zentrale Aufgabe dieser Arbeit war deshalb auch, einen neuen Patch zu
entwickeln, der die Unterstützung der Stamp9G20 Plattform der aktuellen Version
des U-Boots hinzufügt. Und dazu gehörte auch dafür zu sorgen, dass dieser Patch
in den offiziellen U-Boot Repository Baum aufgenommen wird.

\subsection {Der Linux Kernel}
\label{sec:linux}

Der Linux Kernel in all seiner ungeheuren Komplexität beinhaltet einige
mächtige Werkzeuge mit denen es komfortabel möglich ist eine Kernel
Konfiguration zu erstellen. Das Bekannteste dieser Werkzeuge ist wahrscheinlich
die \texttt{kconfig} Oberfläche \texttt{menuconfig}. Mit Hilfe dieser auf
NCurses basierten, einfachen \ac{GUI} ist es komfortabel möglich, innerhalb
einer baumähnlichen Menüstruktur all die Optionen und Module zusammen mit ihren
Abhängigkeiten zu konfigurieren. \autoref{fig:menuconfig} zeigt einen
Screenshot dieser \ac{GUI}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.9\textwidth]{menuconfig}
  \caption{Die \texttt{menuconfig}-GUI des Linux Kernels}
  \label{fig:menuconfig}
\end{figure}

Während es im Bereich der PC-Architektur üblich ist, den Linux Kernel über eben
diese \ac{GUI} an die eigenen Bedürfnisse anzupassen, ist es bei den CPU-Boards
inzwischen Standard, zusammen mit den Kernelanpassungen auch eine vordefinierte
Konfigurationsdatei (\texttt{devconf} genannt) in den Kernel-Tree aufzunehmen,
die alle notwendigen Einstellungen vornimmt, um einen Kernel mit der
notwendigen Hardwareunterstützung kompilieren zu können, der zumindest
bootfähig ist.

Wie in \autoref{sec:hardware} erläutert, war die offizielle Mainline Kernel
Unterstützung für das Stamp9G20-Board eines der Hauptkriterien für die Auswahl
dieser Hardware, dementsprechend gab es hier auch kaum Überraschungen. Ein mit
der von taskit bereitgestellten \texttt{devcon}-Datei kompilierter Kernel ist
sofort lauffähig. Zwar sind inzwischen einige Optionen in dieser Datei
veraltet, aber das Kernel Konfigurationssystem \texttt{kconfig} ist flexibel
genug, damit zurecht zu kommen.

Die schwierigere Aufgabe war hier, die entsprechende Kernel Konfiguration
mittels des Poky Buildsystems zu erhalten. Das Yocto Project hat einen sehr
komplexen Weg gewählt, um die Kernelkonfiguration zu erstellen. Die Idee
dahinter ist, gewisse Kernel Feature, wie z.B. \ac{USB}-Support, in sogenannten
\texttt{*.scc} Dateien vorkonfiguriert bereitzustellen. Dadurch kann der
Entwickler eines BSP-Layers diese Features ganz einfach durch einen
\texttt{include} dieser \texttt{*.scc}-Dateien in seine
Kernelkonfiguration aufnehmen. Die Schwierigkeit entsteht dadurch, dass noch
keine Werkzeuge zur Verfügung stehen, die eine bestehende
Kernelkonfigurationsdatei parsen können und einem einen Überblick darüber
geben, welche der verwendeten Optionen ausgelagert in \texttt{*.scc}-Dateien
zur Verfügung stehen. So müssen diese Konfigurationsdateien, welche oft mehrere
hundert Optionen enthalten, per Hand auf Dubletten mit Optionen in genutzten
\texttt{*.scc}-Dateien durchforstet werden.

So interessant dieser Ansatz auch sein kann, haftet ihm doch der schale
Verdacht an, dass hier versucht wird, das \texttt{kconfig}-System des
Kernels neu zu erfinden. Eine solche Anstrengung scheint, in Anbetracht der
gewaltigen Komplexität des Linux Kernels, doch recht gewagt. Eventuell wäre es
sinnvoller, hier zu versuchen, das \texttt{kconfig}-System direkt zu nutzen.

Im Gegensatz dazu, ist die vom Yocto Project bereitgestellte Methodik,
Anpassungen am Quellcode des Kernels vorzunehmen, sehr gut durchdacht! Da
jedoch das Stamp9G20-Board direkt vom Mainline Kernel unterstützt wird, war es
nicht notwendig dieses System aktiv zu nutzen. Der Vollständigkeit wegen soll
es hier jedoch in angebrachter Kürze erläutert werden. Das Yocto Project nutzt
hier die Möglichkeiten des \ac{DVCS} \texttt{git} und stellt einen um
spezielle Branches erweiterten Fork des Linux Kernel Repositories bereit.

Das offizielle Linux Git Repository enthält nur eine Branch namens
\enquote{master}. Sobald Linus Torvalds eine neue Version des Linux Kernels für
Release bereit hält, bekommt diese Version einen Tag (z.b v3.4). Dieser Tag
dient dann dem Yocto Linux Kernel als sogenannter Branchpoint. Das heißt, es
wird ein Clone des Linux Kernels erstellt und eine neue Branch basierend auf
die Revision mit dem Tag v3.4 erstellt. Für die Version 3.4 heist diese Branch
\texttt{standard/base}. Müssen jetzt für ein BSP-Layer Anpassungen am Kernel
vorgenommen werden, wird basierend auf dieser Branch wiederum eine neue Branch
erstellt. Für den BSP-Layer \enquote{arm-versatile-926ejs} heißt diese Branch
dann zum Beispiel \texttt{standard/arm-versatile-926ejs}. Zusätzlich zu den
BSP-Branches gibt es noch die \texttt{meta} Branch; sie enthält die
angesprochenen \texttt{*.scc}-Dateien und einige für die Kernelkonfiguration
notwendige Tools.

Beim Erstellen eines Yocto Kernels muss dementsprechend angegeben werden,
welche Branch genutzt werden soll. Das Erstellen eines Kernels läuft dann wie
folgt ab:

\begin{enumerate}[label=\arabic*.,itemsep=0.5ex]
  \item Klonen des Yocto Linux Kernels
  \item Erstellen einer neuen lokalen Branch, basierend auf der gewählten
    BSP-Branch.
  \item Danach wird diese Branch mit der \texttt{meta}-Branch gemerged, um
    die \texttt{*.scc}-Dateien und Yocto Kernel Konfigurations-Tools
    verfügbar zu machen.
  \item Jetzt wird aus den Metadaten des BSP-Layers und mit Hilfe der
    \texttt{*.scc}-Dateien eine Kernelkonfigurationsdatei erstellt.
  \item Nun kann der Kernel mittels der Cross-Toolchain kompiliert werden.
\end{enumerate}

\subsection {Das RootFS}
\label{sec:rootfs}

Die letzte grundlegende Komponente eines Linux Systems, die hier besprochen
werden soll, ist das \ac{RootFS}. Dabei handelt es sich um ein Dateisystem, das
die gesamte Software, sowie alle Daten des späteren Linux Systems enthält. Wie
am Ende der Liste der Bootschritte in \autoref{sec:comp} angesprochen, endet
der Linux Kernel Bootprozess damit, dass das Programm \texttt{init}
gestartet wird. \texttt{init} ist damit auch das erste Programm, das im
sogenannten Userspace ausgeführt wird. Der Userspace ist im Gegensatz zum
Kernelspace eine unterprivilegierte execution-Umgebung, in der z.B. der direkte
Zugriff auf die Hardware eines Systems nicht mehr möglich ist. Der
\texttt{init}-Prozess ist dafür zuständig, die notwendigen Systemdienste zu
starten. Also, z.B. das \texttt{getty} Programm, das eine Login-Shell zur
Verfügung stellt oder auch den Syslog Deamon, der sich um das Logging diverser
Systemmeldungen kümmert.

Im Falle der GlobeLog II Applikation enthält das hier entwickelte \ac{RootFS}
natürlich auch die entsprechenden Bibliotheken für die Ansteuerung der
Feuchtemesssensorik und die Python Laufzeitumgebung. Außerdem sollen einige
vordefinierte Benutzeraccounts verfügbar sein. Auch diese können im Rahmen des
Yocto Buildsystems erstellt werden.

Das Yocto Buildsystem bietet die Möglichkeit ganz genau festzulegen, welche
Komponenten ein solches \ac{RootFS} enthält, eine deutlich komfortablere Option
ist allerdings, ein von dem Yocto-Layer vordefiniertes minimales \ac{RootFS}
Image zu verwenden und lediglich die Komponenten hinzuzufügen, die für die
gewünschte Applikation noch fehlen.

Für das fertige Dateisystem kann unter einigen Ausgabeformaten gewählt werden,
ein übliches Format für die Verwendung auf einem Flash-Speicher ist das JFFS2
Dateisystem, welches auch im Rahmen dieser Arbeit zum Einsatz kam
\cite{woodhouse2001}.

% vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab:
