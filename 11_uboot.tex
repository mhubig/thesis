%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\subsection {Der U-Boot Patch}
\label {sec:uboot-patch}

Der Bootloader U-Boot wurde in \autoref{sec:uboot} schon vorgestellt. Was
diesen Bootloader, besonders im Rahmen des Development Prozesses so wichtig
macht, ist die Möglichkeit, ein komplettes Linuxsystem einschließlich Kernel
und \ac{RootFS} aus dem Netzwerk zu starten. Das beschleunigt die einzelnen
Iterationsschritte des Development Prozesses erheblich, da es nicht mehr
notwendig ist, bei jeder Änderung an Kernel oder \ac{RootFS} das CPU-Board neu
zu flashen, um die Änderungen zu testen. Konkret lässt sich das U-Boot so
konfigurieren, dass sich damit solche Tests komplett automatisieren lassen.

Auch im Rahmen des Endproduktes bietet das U-Boot einige sehr wichtige
Funktionen. Theoretisch ist auch At91Bootstrap in der Lage, den Linux Kernel
aus dem Flash zu laden und auszuführen, allerdings muss dafür die genaue Größe
und die Speicherposition des Kernels zum Zeitpunkt des Kompilierens bekannt
sein. U-Boot erlaubt es, diese Angaben zum Zeitpunkt des Ladens zu übergeben,
was wichtig ist, sollte sich die Größe des Kernel nach einem Update geändert
haben. Ein weiterer Vorteil ist, dass U-Boot den Flashspeicher beschreiben
kann, dies ermöglicht den Entwurf einer sehr komfortablen
\ac{OTA}-Update-Strategie für den späteren Einsatz beim Kunden.

Aus diesen Gründen war schon zu Anfang klar, dass eine aktuelle und
funktionierende U-Boot Version eine der wichtigsten Grundvoraussetzungen für
das GlobeLog II Projekt sein würde.

Der interne Aufbau des U-Boots gliedert sich in fünf wichtige Bereiche:

\begin{description}
  \item [arch]
  Dieser Bereich enthält Architektur und CPU spezifischen Code. Für das
  Stamp9G20-Board wird hier ein vorgegebener Rahmen für die Startprozedur
  des U-Boots definiert. Diese besteht aus einer Abfolge von
  Funktionsaufrufe, die bei jedem unterstützten CPU-Board gleich sind.

  \item [board]
  Dieser Bereich enthält für jedes Board einen eigenen Ordner mit zumindest
  einer c-Datei (dem Boardfile), in der die im \texttt{arch}-Bereich
  vorgegebenen Funktionen mit dem aktuellen Startcode für das Board überladen
  werden.

  \item [include]
  Hier findet sich für jedes unterstützte Board eine Header-Datei, die
  verschiedene von den Boardfiles benötigte Werte definiert, zusätzlich
  können hier die gewünschten Features aktiviert werden.

  \item [driver]
  Hier ist der Code für die verschiedenen Hardwarekomponenten zu finden, die
  von U-Boot unterstützt werden. Beispiele dafür sind NAND-Flash Chips,
  USB-Ports oder auch Netzwerk Schnittstellen.

  \item [lib]
  Hier findet sich der Quellcode für die Software-Features des U-Boots, z.B.
  die Scripting Funktionalität oder die Kommandozeile.
\end{description}

Um das U-Boot auf ein neues CPU-Board zu portieren, ist es angeraten, die
Konfiguration eines ähnlichen Boards als Basis zu nehmen und darauf aufzubauen.
Im Falle des Stamp9G20-Boards gibt es ja schon den alten Patch von taskit, der,
zumindest bis vor zwei Jahren, noch funktioniert hatte. Leider hat sich
inzwischen die interne Struktur des U-Boots so gravierend geändert, dass sich
nur wenig davon wiederverwenden ließ.

Als bessere Basis bot sich die Konfiguration des Evaluation Boards für den
AT91SAM9G20 SoC an, weswegen der Stamp9G20 Port fast komplett darauf aufbaut.
Der eigentliche Port besteht im Wesentlichen aus zwei Dateien
\texttt{stamp9g20.h} und \texttt{stamp9g20.c}. Nachfolgend sollen deshalb
einige wichtige Abschnitte daraus kommentiert werden.

\begin{lstlisting} [caption={Init-Sequenz des für ARM-Boards},
  label=lst:board, language=C, gobble=2]
% init_fnc_t *init_sequence[] = {
%     arch_cpu_init,                      /* basic arch dependent setup */
% #ifdef CONFIG_BOARD_EARLY_INIT_F
%     board_early_init_f,
% #endif
% #ifdef CONFIG_OF_CONTROL
%     fdtdec_check_fdt,
% #endif
%     timer_init,                         /* initialize timer */
% #ifdef CONFIG_BOARD_POSTCLK_INIT
%     board_postclk_init,
% #endif
% #ifdef CONFIG_FSL_ESDHC
%     get_clocks,
% #endif
%     env_init,                           /* initialize environment */
%     init_baudrate,                      /* initialze baudrate settings */
%     serial_init,                        /* serial communications setup */
%     console_init_f,                     /* stage 1 init of console */
%     display_banner,                     /* say that we are here */
% #ifdef CONFIG_DISPLAY_CPUINFO
%     print_cpuinfo,                      /* display cpu info + speed */
% #endif
% #ifdef CONFIG_DISPLAY_BOARDINFO
%     checkboard,                         /* display board info */
% #endif
% #if defined(CONFIG_HARD_I2C) || defined(CONFIG_SOFT_I2C)
%     init_func_i2c,
% #endif
%     dram_init,                          /* configure avail. RAM banks */
%     NULL,
% };
\end{lstlisting}

\autoref{lst:board} zeigt anhand der C-Struktur \texttt{init\_sequence} den
Ablauf des U-Boot Startprozesses. Einige der dort gelisteten Funktionsaufrufe
können, wie z.B. \texttt{CONFIG\_BOARD\_EARLY\_INIT\_F} in Zeile 3, über
Präprozessoranweisungen in der Datei \texttt{stamp9g20.h} aktiviert werden.

In Zeile 7 des \autoref{lst:init_f_dev} ist die entsprechende Definition für
die angesprochene Präprozessoranweisung zu sehen. \autoref{lst:init_f} zeigt
dementsprechend die eigentliche damit aktivierte Funktion aus
\texttt{stamp9g20.c}. Alle Funktionen, die wie \texttt{board\_early\_init\_f}
mit \texttt{\_f} enden, werden ausgeführt, bevor U-Boot ans obere Ende des
SDRAM-Speichers relokalisiert wurde. \texttt{board\_early\_init\_f} ist dafür
zuständig, die Peripheral Clock Controller zu aktivieren und damit die Geräte
der Peripherie, wie die Netzwerk Schnittstelle oder die serielle Schnittstelle
zu aktivieren.

\begin{lstlisting} [caption={Beispiel für Präprozessordefinitionen aus
  \texttt{stamp9g20.h}}, label=lst:init_f_dev, language=C, gobble=2]
% /* Misc CPU related settings */
% #define CONFIG_ARCH_CPU_INIT        /* call arch_cpu_init() */
% #define CONFIG_CMDLINE_TAG          /* pass commandline to Kernel */
% #define CONFIG_SETUP_MEMORY_TAGS    /* pass memory defs to kernel */
% #define CONFIG_INITRD_TAG           /* pass initrd param to kernel */
% #define CONFIG_SKIP_LOWLEVEL_INIT   /* U-Boot is loaded by a bootloader */
% #define CONFIG_BOARD_EARLY_INIT_f   /* call board_early_init_f() */
% #define CONFIG_DISPLAY_CPUINFO      /* display CPU Info at startup */
\end{lstlisting}

\begin{lstlisting} [caption={Die Funktion \texttt{board\_early\_init\_f} aus
  \texttt{stamp9g20.c}}, label=lst:init_f, language=C, gobble=2]
% int board_early_init_f(void)
% {
%     struct at91_pmc *pmc = (struct at91_pmc *)ATMEL_BASE_PMC;
%
%     /* Enable clocks for all PIOs */
%     writel((1 << ATMEL_ID_PIOA) | (1 << ATMEL_ID_PIOB) |
%         (1 << ATMEL_ID_PIOC), &pmc->pcer);
%
%     return 0;
% }
\end{lstlisting}

In \texttt{stamp9g20.c} sind außer \texttt{board\_early\_init\_f} noch sechs
weitere Funktionen definiert, deren Funktionalität hier kurz zusammengefasst
werden sollen:

\begin{description}[style=nextline]
  \item [\texttt{stamp9G20\_nand\_hw\_init}]
  Zuständig für die Initialisierung und Konfiguration des NAND-Flash Speichers.

  \item [\texttt{stamp9G20\_macb\_hw\_init}]
  Zuständig für die Initialisierung und Konfiguration der Netzwerk
  Schnittstelle.

  \item [\texttt{board\_postclk\_init}]
  Diese spezielle Funktion läuft ähnlich früh wie
  \texttt{board\_early\_init\_f} allerdings erst nach der Aktivierung der
  Timer. Da diese wichtig sind für die Funktionalität der seriellen
  Schnittstelle, wird diese hier aktiviert.

  \item [\texttt{board\_init}]
  Diese Funktion ist generell für die Initialisirung der Hardware zuständig,
  und führt unter anderem die Init-Funktionen für Netzwerk und NAND aus.

  \item [\texttt{dram\_init}]
  Initialisiert den SDRAM-Speicher, wird kurz vor der Relokalisierung
  ausgeführt.

  \item [\texttt{board\_eth\_init}]
  Aktiviert den Netzwek Protokoll Treiber.
\end{description}

\autoref{lst:stamp-textbase} zeigt eine der wichtigsten Optionen aus der Datei
\texttt{stamp9g20.h}. Die Adresse, an die das U-Boot vom \ac{SPL}-At91Bootstrap
kopiert und gestartet wird. Diese Einstellung muss natürlich in Bootstrap und
U-Boot identisch sein.

\begin{lstlisting} [caption={Setzen der U-Boot Speicheradresse in der
  \texttt{stamp9g20.h}-Datei}, label=lst:stamp-textbase, language=C, gobble=2]
% /*
%  * Setting the RAM Address from where the U-Boot gets started by the SLP.
%  * Warning: changing CONFIG_SYS_TEXT_BASE here requires adapting the SPL
%  * Use a pure hex number here!
%  */
% #define CONFIG_SYS_TEXT_BASE        0x23f00000
\end{lstlisting}

\autoref{lst:stamp-clock} zeigt die Configuration der CPU-Clocks. Diese
Einstellungen sind wichtig, damit diverse Timer-Werte genau passen.

\begin{lstlisting} [caption={Configuration der CPU-Clocks in der
  \texttt{stamp9g20.h}-Datei}, label=lst:stamp-clock, language=C, gobble=2]
% /* ARM asynchronous clock */
% #define CONFIG_SYS_AT91_SLOW_CLOCK  32768       /* slow clock xtal   */
% #define CONFIG_SYS_AT91_MAIN_CLOCK  18432000    /* 18.432MHz crystal */
% #define CONFIG_SYS_HZ               1000        /* 1ms resolution    */
\end{lstlisting}

\autoref{lst:stamp-sdram} zeigt die Konfiguration des SDRAM-Speichers, da
U-Boot schon im SDRAM-Seicher läuft, muss hier keine Initialisierung mehr
erfolgen, sondern lediglich Adressbereich und Größe angegeben werden.

\begin{lstlisting} [caption={SDRAM Einstellungen in der
  \texttt{stamp9g20.h}-Datei}, label=lst:stamp-sdram, language=C, gobble=2]
% /*
%  * SDRAM: 1 bank, 64 MB, base address 0x20000000
%  * Already initialized before u-boot gets started.
%  */
% #define CONFIG_NR_DRAM_BANKS        1
% #define CONFIG_SYS_SDRAM_BASE       ATMEL_BASE_CS1
% #define CONFIG_SYS_SDRAM_SIZE       (64 << 20)
\end{lstlisting}

\autoref{lst:stamp-nand} zeigt die Einstellungen, welche später, unter anderem
während der Board Initialisierung, von der Funktion
\texttt{stamp9G20\_nand\_hw\_init} genutzt wird um den NAND-Flash Speicher
korrekt zu konfigurieren. Dies ist wichtig, da das U-Boot nicht nur lesenden,
sondern auch schreibenden Zugriff auf darauf benötigt.

\begin{lstlisting} [caption={Konfiguration des NAND-Flash Speichers in der
  \texttt{stamp9g20.h}-Datei}, label=lst:stamp-nand, language=C, gobble=2]
% /* NAND flash settings */
% #define CONFIG_NAND_ATMEL
% #define CONFIG_SYS_NO_FLASH
% #define CONFIG_SYS_MAX_NAND_DEVICE  1
% #define CONFIG_SYS_NAND_BASE        ATMEL_BASE_CS3
% #define CONFIG_SYS_NAND_DBW_8
% #define CONFIG_SYS_NAND_MASK_ALE    (1 << 21)
% #define CONFIG_SYS_NAND_MASK_CLE    (1 << 22)
% #define CONFIG_SYS_NAND_ENABLE_PIN  AT91_PIN_PC14
% #define CONFIG_SYS_NAND_READY_PIN   AT91_PIN_PC13
\end{lstlisting}

In \autoref{lst:stamp-kernel} wird die Standard Adresse gesetzt, an die der
Linux Kernel kopiert und gestartet wird. Diese Adresse wird, wenn zusätzlich
die Option \texttt{CONFIG\_SETUP\_MEMORY\_TAGS} gesetzt ist, automatisch an den
Linux Kernel übergeben.

\begin{lstlisting} [caption={Standard Speicheradresse des Linux Kernels},
  label=lst:stamp-kernel, language=C, gobble=2]
% /*
%  * RAM Memory address where to put the
%  * Linux Kernel before starting.
%  */
% #define CONFIG_SYS_LOAD_ADDR        0x22000000
\end{lstlisting}

In \autoref{lst:stamp-nand-partitions} wird die Unterteilung des NAND-Flash
Speichers in logische Partitionen spezifiziert. Über die Option
\texttt{CONFIG\_EXTRA\_ENV\_SETTINGS} kann die Umgebungsvariable
\texttt{mtdparts} gesetzt werden, die sich dann später dem Linux Kernel als
Startparameter übergeben lässt. Der Linux \ac{MTD}-Treiber übernimmt diese
Angaben und ermöglicht die transparente Nutzung dieser Partitionen als
Festplatten.

\pagebreak

\begin{lstlisting} [caption={Partitionierung des NAND-Flash Speichers},
  label=lst:stamp-nand-partitions, language=C, gobble=2]
% /*
%  * The NAND Flash partitions:
%  * ==========================================
%  * 0x0000000-0x001ffff -> 128k, bootstrap
%  * 0x0020000-0x005ffff -> 256k, u-boot
%  * 0x0060000-0x007ffff -> 128k, env1
%  * 0x0080000-0x009ffff -> 128k, env2 (backup)
%  * 0x0100000-0x06fffff ->   6M, kernel
%  * 0x0700000-0x8000000 -> 121M, RootFS
%  */
% #define CONFIG_ENV_IS_IN_NAND
% #define CONFIG_ENV_OFFSET           ((128 + 256) << 10)
% #define CONFIG_ENV_OFFSET_REDUND    ((128 + 256 + 128) << 10)
% #define CONFIG_ENV_SIZE             (128 << 10)
% /*
%  * Predefined environment variables.
%  * Usefull to define some easy to use boot commands.
%  */
% #define CONFIG_EXTRA_ENV_SETTINGS  \
%    "mtdparts=atmel_nand:           \
%        128k(bootstrap)ro,          \
%        256k(uboot)ro,              \
%        128k(env1)ro,128k(env2)ro,  \
%        6M(linux),-(rootfs)rw\0"
\end{lstlisting}

\autoref{lst:stamp-features} letztendlich zeigt, wie sich über die Header
Datei unterschiedliche U-Boot Features aktivieren bzw. deaktivieren lassen.

\begin{lstlisting} [caption={Auswahl zusätzlicher Features in der
  \texttt{stamp9g20.h}-Datei}, label=lst:stamp-features]
% /* Command line & features configuration */
% #include <config_cmd_default.h>
% #undef CONFIG_CMD_FPGA
% #undef CONFIG_CMD_IMI
% #undef CONFIG_CMD_IMLS
% #undef CONFIG_CMD_LOADS
%
% #define CONFIG_CMD_NAND
% #define CONFIG_CMD_USB
% #define CONFIG_CMD_FAT
% #define CONFIG_CMD_LED
\end{lstlisting}

In \autoref{sec:uboot} wurde schon darauf eingegangen, dass ein Ziel dieser
Arbeit ist, nicht nur das U-Boot auf die Stamp9G20 Plattform zu portieren,
sondern diesen Port offiziell in das zentrale U-Boot Repository aufnehmen zu
lassen. Der übliche Weg dahin ist es, die Anpassungen am U-Boot zu einem Patch
zusammenzufassen und auf der offiziellen Mailingliste des U-Boots zur Review
einzureichen. Ein solcher Patch wird dann vom Maintainer des entsprechenden
Custodian Repositories begutachtet und entweder akzeptiert und in das
Repository aufgenommen, oder abgelehnt, was meist bedeutet, dass der Author des
Patches nochmal nacharbeiten muss und den Patch danach noch einmal zur
Begutachtung einreichen kann. Dabei sind die Kriterien recht streng. Der Patch
muss nicht nur guten Code enthalten, sondern auch dem Linux Kernel
Codingstyle\footnote{Siehe:
\url{http://www.kernel.org/doc/Documentation/CodingStyle}} folgen. Der Patch,
der hier beschriebenen Anpassungen, ging durch sechs Reviews, bis er offiziell
akzeptiert wurde. Inzwischen ist er im Main Repository verfügbar, was heißt,
dass voraussichtlich der Support für die Stamp9G20 Plattform mit der Version
2012.10 des U-Boots offiziell sein wird.

Insgesamt war die Portierung des U-Boots der aufwendigste Teil dieses
Projektes. Dies lag vor allem daran, dass es sich dabei um reine
Mikrocontroller Programmierung handelte, was genaue Kenntnisse der internen
Funktionalität des Stamp9G20 voraussetzt. Da der hier entwickelte Patch
allerdings inzwischen Teil des offiziellen U-Boots ist, war es gleichzeitig
aber auch der nachhaltigste Teil dieser Arbeit.

% vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab:
