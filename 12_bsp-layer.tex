%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\subsection {BSP-Layer für das Yocto Buildsystem}
\label {sec:bsp-layer}

In \autoref{sec:yocto} wurde der grundsätzliche Aufbau des Yocto Buildsystems
besprochen und auch die Möglichkeiten diskutiert mit Hilfe von
\texttt{*.bbappend} und \texttt{*.bbclass} Dateien, sowie der
Layer-Struktur eine Vererbungsstruktur aufzubauen, die sehr flexibel ist und es
erlaubt, jede einzelne Komponente dieses Systems wiederzuverwenden. In diesem
Abschnitt soll nun der Aufbau des meta-stamp9g20 \ac{BSP}-Layers dokumentiert
werden.

Der \ac{BSP}-Layer sollte all die Softwarepakete, Einstellungen und Patches
zusammenfassen, die notwendig sind, um die komplette Hardware eines CPU-Boards
nutzen zu können. Dazu gehören in erster Linie der \ac{SPL}, der Bootloader und
natürlich der Linux Kernel. Aber auch Bibliotheken und Programme, um auf
bestimmte Hardwarefunktionen zuzugreifen.

Die erste wichtige Datei eines BSP-Layers ist \texttt{conf/layers.conf}
(\autoref{lst:layer.conf}). Sie markiert die gesamte Verzeichnisstruktur als
Yocto Layer, erlaubt es die Priorität des Layer zu spezifizieren und definiert
eine sogenannte Pattern-Such-Maske (Zeilen 5+6), mit der sich die enthaltenen
\texttt{*.bb} und \texttt{*.bbappend} Dateien finden lassen.

\begin{lstlisting}[caption={Die \texttt{layer.conf} Datei},
  label=lst:layer.conf, language=C, gobble=2]
% # We have a conf and classes directory, add to BBPATH
% BBPATH := "${BBPATH}:${LAYERDIR}"
%
% # We have a recipes directory, add to BBFILES
% BBFILES := "${BBFILES} ${LAYERDIR}/recipes-*/*/*.bb \
%             ${LAYERDIR}/recipes-*/*/*.bbappend"
%
% BBFILE_COLLECTIONS += "${MACHINE}"
%
% BBFILE_PATTERN_stamp9g20 := "^${LAYERDIR}/"
% BBFILE_PRIORITY_stamp9g20 = "6"
%
% BBFILE_PATTERN_portuxg20 := "^${LAYERDIR}/"
% BBFILE_PRIORITY_portuxg20 = "6"
\end{lstlisting}

Die nächste wichtige Datei des BSP-Layers findet sich unter
\texttt{conf/machine} und hat immer den Namen des von dem Layer
unterstützten CPU-Boards. In diesem Fall ist das \texttt{portuxg20} und
\texttt{stamp9g20}. \autoref{lst:machine.conf} zeigt einen Ausschnitt einer
solchen Datei. Da die beiden unterstützten CPU-Boards sich nur in wenigen
Details unterscheiden, ist die meiste Funktionalität in die Datei
\texttt{taskit.inc} (\autoref{lst:taskit.inc}) ausgelagert. In Zeile 2 wird
hier eine vom Yocto Buildsystem bereitgestellte Datei eingebunden, die alle
notwendigen Einstellungen für die ARM926EJS CPU enthält. In den Zeilen 11+12
werden die gewünschten Formate für das \ac{RootFS} angegeben und die Kernel
Version wird in den Zeilen 15+16 festgelegt.

\begin{lstlisting}[caption={Die \texttt{machine.conf} Datei},
  label=lst:machine.conf, language=sh, gobble=2]
% # Include the common taskit configuration
% include taskit.inc
%
% # u-boot build configuration
% UBOOT_MACHINE = "portuxg20_config"
\end{lstlisting}

\begin{lstlisting}[caption={Die \texttt{taskit.inc} Datei},
  label=lst:taskit.inc, language=sh, gobble=2]
% # Enable arm926ejs specific processor optimizations
% include conf/machine/include/tune-arm926ejs.inc
%
% # Ship all kernel modules by default
% MACHINE_EXTRA_RRECOMMENDS = " kernel-modules"
%
% # Allow for MMC booting
% EXTRA_IMAGEDEPENDS += "u-boot at91bootstrap"
%
% # Set the image types to create
% IMAGE_FSTYPES += "tar.bz2 jffs2"
% EXTRA_IMAGECMD_jffs2 = "-lnp "
%
% # Kernel version to use
% PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
% PREFERRED_VERSION_linux-yocto ?= "3.2%"
%
% # u-boot build settings
% UBOOT_ENTRYPOINT = "0x20008000"
% UBOOT_LOADADDRESS = "0x20008000"
%
% # some extra machine features
% MACHINE_FEATURES = "usbgadget usbhost vfat"
\end{lstlisting}

Im Verzeichnis \texttt{recipes-bsp/at91bootstrap} findet sich die *.bb Datei
für den \ac{SPL} At91Bootstrap (\autoref{lst:at91bootstrap_3.0.bb}). Auch hier
ist wieder einige Funktionalität in einer *.inc-Datei ausgelagert. Interessant
ist hier vor allem die \texttt{SRC\_URI} Variable (im Ausschnitt gekürzt).  Hier
wird zum einen die Quelle des Programms angegeben, zum anderen aber auch
Patches, die dem BSP-Layer direkt beiliegen. Die als letztes angegeben Datei
\texttt{defconfig} enthält die Konfigurationsoptionen die später (siehe
\autoref{lst:at91bootstrap_3.0.inc}) von der Funktion \texttt{do\_compile}
genutzt werden, um die Software vor dem Kompilieren zu konfigurieren.

\begin{lstlisting}[caption={Ein Ausschnitt der \texttt{at91bootstrap\_3.0.bb}
  Datei}, label=lst:at91bootstrap_3.0.bb, language=sh, gobble=2]
% require at91bootstrap_3.0.inc
%
% SRC_URI[md5sum] = "9426fc7962b68de70ec3941c3a6fd351"
% SRC_URI[sha256sum] = "dbf30f1d86bf0e2936ed5sd920..."
%
% SRC_URI = "\
%     ftp://ftp.linux4sam.org/pub/Android4SAM/.../bootstrap30.tar.gz \
%     file://0018-Added-support-for-Micron-MT29F1G08ABB-NAND-flash.patch \
%     file://0019-Extracted-some-copyright-informations-from-main.c.patch \
%     file://defconfig \
%     "
\end{lstlisting}

\begin{lstlisting}[caption={Die Funktion \texttt{do\_compile} aus der Datei
  \texttt{at91bootstrap\_3.0.inc}}, label=lst:at91bootstrap_3.0.inc,
  language=Python, gobble=2]
% do_compile () {
%     unset LDFLAGS
%     unset CFLAGS
%     unset CPPFLAGS
%     oe_runmake mrproper
%     rm -Rf ${S}/binaries
%     cp ${S}/../defconfig ${S}/.config
%     oe_runmake
% }
\end{lstlisting}

Die Datei \texttt{uboot\_2012.07.bb} für den U-Boot Bootloader findet sich im
BSP-Layer Verzeichnis \texttt{recipes-bsp/u-boot}. In
\autoref{lst:uboot_2012.07.bb} ist ebenfalls die \texttt{SRC\_URI} Variable
interessant. Hier ist zu sehen, wie als Software-Quelle ein \texttt{git}
Repository angegeben werden kann.  Die \texttt{git}-\ac{URI} ermöglicht unter
anderem auch die Angabe der gewünschten Branch. Die \texttt{SRCREV} Variable
enthält den Hash der Revision. Zusätzlich sind hier noch die Original-Patches
enthalten, da die Unterstützung für die Stamp9G20 Plattform wohl erst in der
nächsten stabilen Version (2012.10) der U-Boots enthalten sein wird.

\begin{lstlisting}[caption={Ein Ausschnitt der Datei
  \texttt{uboot\_2012.07.bb}}, label=lst:uboot_2012.07.bb,
  language=sh, gobble=2]
% # This revision corresponds to the tag "v2012.07". We use the revision
% # in order to avoid having to fetch it from the repo during parse
% SRCREV = "190649fb4309d1bc0fe7732fd0f951cb6440f935"
%
% PV = "v2012.07+git${SRCPV}"
% PR = "r1"
%
% SRC_URI = "git://git.denx.de/u-boot.git;branch=master;protocol=git     \
%     file://0001-Enable-the-EMAC-clock-in-at91_macb_hw_init.patch       \
%     file://0002-at91-Add-support-for-taskit-AT91SAM9G20-boards.patch   \
%     file://0003-arm-Adds-board_postclk_init-to-the-init_sequence.patch \
%     file://0004-Fixes-the-crippled-console-output-on-PortuxG20.patch   \
%     "
\end{lstlisting}

Das Rezept für die letzte wichtige Komponente, den Kernel, findet sich in der
\texttt{.bbappend} Datei \texttt{linux-yocto\_3.2.bbappend} im Verzeichnis
\texttt{recipes-kernel/linux}. Hier wird in den Zeilen 8 und 12 jeweils die
Branch angegeben, auf welcher der spätere Kernel basieren soll. Die
\texttt{SRC\_URI\_append} Statements listen die \texttt{*.scc} Dateien, welche
Angaben über die Kernel-Konfigurationsfragmente enthalten. \autoref{lst:scc}
zeigt zwei solcher \texttt{*.scc} Dateien, die aufeinander aufbauen. Gut zu
sehen ist hier, dass die erste Datei \texttt{portuxg20-standard.scc} einige
Variablen deklariert und am Ende die zweite Datei \texttt{portuxg20.scc}
einbindet. Die zweite Datei letztendlich spezifiziert dann die für die
Kernelkonfiguration zu nutzenden \texttt{*.cfg} Dateien.

\begin{lstlisting}[caption={Das Linux Kernel Rezept
  \texttt{linux-yocto\_3.2.bbappend}}, label=lst:linux-yocto_3.2.bbappend,
  language=sh, gobble=2]
% FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
%
% PR := "${PR}.1"
%
% KEEPUIMAGE = "no"
%
% COMPATIBLE_MACHINE_stamp9g20 = "stamp9g20"
% KBRANCH_stamp9g20  = "standard/default/arm-versatile-926ejs"
% KMACHINE_stamp9g20  = "stamp9g20"
%
% COMPATIBLE_MACHINE_portuxg20 = "portuxg20"
% KBRANCH_portuxg20  = "standard/default/arm-versatile-926ejs"
% KMACHINE_portuxg20  = "portuxg20"
%
% SRC_URI_append_portuxg20 = "\
%         file://portuxg20-standard.scc   \
%         file://portuxg20-preempt-rt.scc \
%         file://portuxg20.scc            \
%         "
%
% SRC_URI_append_stamp9g20 = "\
%         file://stamp9g20-standard.scc   \
%         file://stamp9g20-preempt-rt.scc \
%         file://stamp9g20.scc            \
%         "
\end{lstlisting}

\begin{lstlisting}[caption={Beispiel zweier \texttt{*.scc} Dateien},
  label=lst:scc, language=sh, gobble=2]
% ----- portuxg20-standard.scc ------
% define KMACHINE portuxg20
% define KTYPE standard
% define KARCH arm
% include ktypes/standard
% include portuxg20.scc
%
% --------- portuxg20.scc ----------
% kconf hardware portuxg20.cfg
% kconf hardware hardware.cfg
% kconf non-hardware non-hardware.cfg
% include features/usb-net/usb-net.scc
\end{lstlisting}

Auch der Sourcecode dieses meta-stamp9g20 BSP-Layer steht unter einer
Revisionsverwaltung durch das Programm \texttt{git}. Da dieser Layer allerdings
nicht ohne den Unterbau des Yocto Buildsystems funktionieren kann, ist hier die
Anwendung des Branching Modells \texttt{git-flow} nicht so einfach, da das
Yocto Projekt ein eigenes Branching Model verwendet. Eine mögliche Lösung dafür
soll im \autoref{sec:dev-env} skizziert werden.

% vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab:
