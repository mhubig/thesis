%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\subsection {Buildsysteme für Embedded Linux Distributionen}
\label{sec:buildsys}

In der Einleitung zu dieser Arbeit wurde die Komplexität einer Linux
Distribution schon angerissen. Dieser Abschnitt soll daran anknüpfen und
Werkzeuge vorstellen, mit deren Hilfe es möglich ist eine Linux Distribution
für ein mittelständisches Unternehmen nutz- und wartbar zu machen. Dazu ist es
allerdings nötig, detailliert auf die einzelnen Komponenten eines Linux
Betriebssystems einzugehen.

Um einen Computer zu nutzen, bedarf es grundsätzlich dreier Komponenten. Dem
Bootloader, dem Kernel und einer Zusammenstellung der Tools um den Computer
später bedienen zu können (im Folgenden \acs{RootFS} genannt). Der Bootloader
wird als erste Software von der CPU ausgeführt. Seine typische Aufgabe ist es,
all die Hardware zu aktivieren, die nötig ist um auf den Speicherort des
Kernels zuzugreifen und diesen zu laden. Der Kernel ist dafür zuständig, die
komplette Hardware eines Computers zu aktivieren und Softwareschnittstellen
bereitzustellen, über die später die Softwarekomponenten des Computers auf
diese Hardware zugreifen können. Der Zweck des RootFS ist es, genau die
Software bereitzustellen, welche nötig ist, um die Aufgaben des Compuersystems
erfüllen zu können.

Je detaillierter man diese Komponenten betrachtet, um so komplexer werden sie.
Der Bootloader ist eine zwar meist in der absoluten Größe überschaubare
Software, jedoch muss diese Komponente beim Start des Computers mit sehr
beschränkten Ressourcen, was z.B. den Speicher betrifft, umgehen können.

Der Linux Kernel ist ein gewaltiges Softwareprojekt. 14 Millionen Zeilen Code,
37.000 Quellcode Dateien, tausende von Entwicklern, ca. 30 unterstützte CPU
Plattformen. Linux findet sich auf den kleinsten W-LAN Routern, auf den
unterschiedlichsten PCs und Laptops, auf Handys und sogar auf den größten
Mainframe Servern.

Und letztendlich das RootFS, welches alle Software Pakete zusammenfasst, die
für den Betrieb notwendig sind. Das RootFS einer Linux Distribution beinhaltet
bis zu 2000 unterschiedliche Softwarepakete und selbst eine minimale
Zusammenstellung, die lediglich eine Kommandozeile bereitstellt, umfasst noch
einige hundert Pakete.

All diese Komplexität zu verwalten und wartbar zu halten, ist eine große
Aufgabe und gleichzeitig der Kernaspekt dieser Arbeit. Jede Linux Distribution
verfügt über ein sogenanntes Buildsystem, mit dessen Hilfe sich Kernel und
RootFS automatisiert erstellen lassen. Allerdings liegt der Focus der meisten
Distributionen, wie Ubuntu Linux oder Debian, darauf, möglichst viele
Einsatzszenarien abzudecken. So können diese Distributionen auf Desktops und
Servern genutzt werden, bieten die Möglichkeit unterschiedlichste grafische
Oberflächen zu nutzen und Softwarepakete für Office und Multimedia Anwendungen,
Entwicklungswerkzeuge und vieles mehr.

Die Ansprüche für ein Embedded Device sind grundsätzlich anders. Auf einem
Embedded Device soll möglichst nur die Software installiert sein, die unbedingt
notwendig ist, um den Zweck des Gerätes erfüllen zu können. Jedes zusätzliche
Software Paket kann zu Problemen führen, muss regelmäßig aktualisiert werden
und verschwendet Speicherplatz und Rechenzeit. Aus diesem Grund sollte ein
Buildsystem für Embedded Linux die Möglichkeit bieten, die Auswahl der
Softwarekomponenten sehr feingraduiert festzulegen. Weitere Ansprüche sind die
einfache Weiterverwendbarkeit des konfigurierten Systems für andere Projekte
und andere Hardware.

Es gibt derzeit sechs relevante Buildsysteme für Embedded Linux Distributionen:

\begin{enumerate}[label=\arabic*),itemsep=0.5ex]
  \item Das Debian Buildsystem
    \footnote{\url{http://www.debian.org/doc/manuals/maint-guide}}
  \item Buildroot\footnote{\url{http://buildroot.uclibc.org}}
  \item OpenEmbedded Classic\footnote{\url{http://www.openembedded.org}}
  \item OpenEmbedded Core
  \item ELDK\footnote{\url{http://www.denx.de/wiki/ELDK-5/WebHome}}
  \item Das Yocto Project\footnote{\url{http://yoctoproject.org}}
\end{enumerate}

Ein Vergleich dieser fünf Systeme ergab recht schnell, dass das Buildsystem
Poky des Yocto Projekts, das viel versprechendste Buildsystem darstellt. Das
Debian Buildsystem bietet zwar mächtige Tools für das Erstellen von
Softwarepaketen für ein bestehendes Debian Linux System, allerdings erscheint
es recht komplex, damit ein eigenes komplettes System zu erstellen. Buildroot,
im Gegensatz dazu, ist vor allem dazu ausgelegt, komplette Systeme zu bauen.
Allerdings ist es bei diesem Konzept nicht angedacht, Teile des erstellten
Buildsystems für zukünftige Projekte weiter zu verwenden. OpenEmbedded Classic
wurde inzwischen von OpenEmbedded Core abgelöst und OpenEmbedded Core ist mehr
eine grundlegende Sammlung einzelner Build-Rezepte für Software Pakete, als ein
vollständiges Buildsystem. ELDK letztendlich, war bis zur Version 4 ein
eigenständiges Buildsystem, vor allem für die PowerPC Architektur; seit Version
5 basiert es allerdings nahezu vollständig auf dem Buildsystem des Yocto
Projekts.

\pagebreak

\subsection {Poky -- Das Buildsystem des Yocto Projekts}
\label{sec:yocto}

Das Yocto Projekt ist ein Zusammenschluss unterschiedlicher Firmen und
Entwickler unter dem Schirm der Linux Foundation, mit dem Ziel, eine
Standard-Plattform für das Erstellen von spezialisierten Linux Betriebsystemen
für Embedded Devices zu schaffen. Das Buildsystem des Yocto Projekts heißt Poky
und besteht aus drei Komponenten:

\begin{description}
  \item [Core Layer]
  Eine Sammlung ausgewählter Buildrezepte aus dem Repertoire von Openembedded
  Core für verschiedene Softwarepakete.

  \item [Bitbake]
  Eine in Python geschriebene Software, welche die Buildrezepte auswertet und
  ausführt.

  \item [Yocto Layer]
  Ein Layer aus spezialisierten Buildrezepten, welche die Rezepte aus dem Core
  Layer zu einer Distribution zusammenfügen.
\end{description}

Ein Build Rezept ist eine Textdatei mit der Endung \texttt{.bb}, die eine
Reihe standardisierter Funktionen und Variablen definiert. Die Funktionen, auch
Tasks genannt, werden in einer festgelegten Reihenfolge ausgeführt und
spezifizieren wie und von wo eine Software heruntergeladen wird, wie sie
konfiguriert wird, wie sie zu kompilieren ist, von welchen anderen Paketen sie
abhängt und letztendlich wie daraus ein installierbares Softwarepaket erstellt
werden kann. \autoref{lst:bb} zeigt beispielhaft eine solche Rezept Datei für
die zlib Bibliothek.

\begin{lstlisting} [caption={Beispiel eines bitbake Rezeptes
  (\texttt{zlib\_1.2.7.bb})}, label=lst:bb, language=sh, gobble=2]
% SUMMARY = "Zlib Compression Library"
% DESCRIPTION = "Zlib is a general-purpose, patent-free, lossless data \
% compression library which is used by many different programs."
% HOMEPAGE = "http://zlib.net/"
% SECTION = "libs"
%
% LICENSE = "Zlib"
% LIC_FILES_CHKSUM = "file://zlib.h;beginline=4;endline=23;md5=94d1b5a..."
%
% SRC_URI = "http://www.zlib.net/${BPN}-${PV}.tar.bz2 \
%            file://remove.ldconfig.call.patch \
%            "
% SRC_URI[md5sum]    = "2ab442d1691..."
% SRC_URI[sha256sum] = "49e2e9658df..."
%
% do_configure (){
%     ./configure --prefix=${prefix} --shared --libdir=${libdir}
% }
%
% do_compile (){
%     oe_runmake
% }
%
% do_install() {
%     oe_runmake DESTDIR = ${D} install
% }
\end{lstlisting}

In den Zeilen 16-26 werden die Standard Methoden \texttt{do\_configure()},
\texttt{do\_compile()} und \texttt{do\_install()} überladen und damit die
gesamte Installationsroutine dieser Software automatisiert. Variable, wie z.B.
\texttt{SRC\_URI} in Zeile 10, werden genutzt, um den Quelltext der Software
herunterzuladen. Zusätzlich werden alle Dateien in der \texttt{SRC\_URI} Liste
mit der Endung \texttt{.patch} automatisch auf die heruntergeladene und
entpackte Software angewendet.

Zusätzlich zu den \texttt{.bb} Dateien gibt es noch \texttt{.bbappend} und
\texttt{.bbclass} Dateien. Eine Datei mit dem Namen
\texttt{zlib\_1.2.7.bbappend} könnte genutzt werden, um bestimmte Aspekte der
originalen \texttt{.bb} Datei zu überladen. \autoref{lst:bbappend} zeigt ein
typisches Beispiel einer solchen Datei, bei dem als Download Quelle ein
\texttt{git} Repository und die genaue Revisions-ID (\texttt{SRCREV}) der
gewünschten Version angegeben ist. Wird diese Datei zusätzlich zur \texttt{.bb}
Datei von bitbake gefunden, überlädt sie die ursprüngliche Downloadquelle.

\texttt{.bbclass} Dateien können genutzt werden, um Standardwerte zu
definieren. Mit dem Schlüsselwort \texttt{inherit} können diese Werte dann von
anderen \texttt{.bb} oder \texttt{.bbappend} geerbt werden.

\begin{lstlisting} [caption={Beispiel einer bbappend Datei
  (\texttt{zlib\_1.2.7.bbappend})}, label=lst:bbappend, language=sh, gobble=2]
% SRCREV  = "30a1c7065dc1dc2c2ed68ed403792b660bfdd805"
% SRC_URI = "git://github.com/madler/zlib.git;protocol=git;branch=master \
%           "
\end{lstlisting}

Diese Möglichkeiten werden noch erweitert, durch die Option, sogenannte Layer
zu definieren. Ein Layer in der Yocto-Terminologie ist ein Ordner, der eine
bestimmte Konfigurationsdatei enthält, welche die Priorität des Layers
definiert. Mehrere Layer zusammen ergeben eine Art Stapel, bei dem immer der
Layer mit der höheren Priorität die Werte der Layer mit niedrigerer Priorität
überladen kann. So ist es möglich, die Metadaten in themenzentrische Layer zu
gruppieren und diese für weitere Projekte weiter zu verwenden. In
\autoref{fig:yocto-layer} ist ein typischer Layer Stapel zu sehen. Es wäre
damit zum Beispiel möglich, das gesamte Setup auf eine neue Hardware zu
portieren, indem einfach der BSP-Layer durch einen anderen ersetzt wird.
Ebenso wäre vorstellbar, einen zusätzlichen Layer hinzuzufügen, der spezielle
Debuging Funktionen integriert. Dieser Layer könnte dann beim Erstellen des
Endkundensystems einfach weggelassen werden.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\textwidth]{yocto-layer}
  \caption{Darstellung der Layerstruktur des Yocto-Projekts}
  \label{fig:yocto-layer}
\end{figure}

Um mit dem Poky Buildsystems ein Linux System Image zu erstellen, wird mit
Hilfe von Bitbake eine ganze Kette an Operationen angestoßen. Die erste
Vorbedingung ist das Erstellen eines Buildordners. Dieser enthält eine
Konfigurationsdatei mit den Angaben des Zielsytems, dem zu nutzenden Layer und
einigen Pfadangaben. Poky liefert ein Bash-Skript mit, das dafür sorgt,
notwendige Umgebungsvariabeln zu setzen und den Buildordner zu erstellen.
\autoref{lst:poky-setup} demonstriert das Erstellen eines Build-Ordners und das
Erstellen eines einfachen minimalen Basis Images.

\begin{lstlisting} [caption={Erstellen eines minimalen Linux Images auf der
  Kommandozeile}, label=lst:poky-setup, language=sh, gobble=2]
% $ cd ~/Development
% $ source poky/oe-init-build-env build
% $ bitbake core-image-minimal
\end{lstlisting}

Bitbake parsed im ersten Schritt alle Metadaten und löst die Abhängigkeiten der
Rezepte auf, danach wird eine Toolchain für das Hostsystem erstellt. Diese
Toolchain wird genutzt, um eine Cross-Toolchain für das Zielsystem zu
erstellen. Danach werden die einzelnen Rezepte ausgeführt, die Softwarepakete
heruntergeladen, entpackt, kompiliert, Pakete erstellt und damit letztendlich
das gewünschte Basisimage erstellt. Poky bietet sogar die Möglichkeit, dieses
Image im Anschluss mittels der virtuellen Maschine \texttt{qemu} direkt zu
starten und zu testen.

% vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab:
